package assignment

import bcrypt.AsyncBcrypt
import com.typesafe.scalalogging.StrictLogging
import store.AsyncCredentialStore
import util.Scheduler

import scala.concurrent.{ExecutionContext, Future, Promise, TimeoutException}
import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success, Try}

class Assignment(bcrypt: AsyncBcrypt, credentialStore: AsyncCredentialStore)
                (implicit executionContext: ExecutionContext) extends StrictLogging {

  /**
    * проверяет пароль для пользователя
    * возвращает Future со значением false:
    *   - если пользователь не найден
    *   - если пароль не подходит к хешу
    */
  def verifyCredentials(user: String, password: String): Future[Boolean] = {
    credentialStore.find(user).flatMap {
      case Some(hash) => bcrypt.verify(password, hash)
      case None => Future {
        false
      }
    }
  }

  /**
    * выполняет блок кода, только если переданы верные учетные данные
    * возвращает Future c ошибкой InvalidCredentialsException, если проверка не пройдена
    */
  def withCredentials[A](user: String, password: String)(block: => A): Future[A] = {
    verifyCredentials(user, password).flatMap {
      case true => Future {
        block
      }
      case false => Future.failed(new InvalidCredentialsException)
    }
  }

  /**
    * хеширует каждый пароль из списка и возвращает пары пароль-хеш
    */
  def hashPasswordList(passwords: Seq[String]): Future[Seq[(String, String)]] = {
    val hashes = Future.sequence(passwords.map(bcrypt.hash(_)))
    hashes.map(hash => passwords.zip(hash))
  }

  /**
    * проверяет все пароли из списка против хеша, и если есть подходящий - возвращает его
    * если подходит несколько - возвращается любой
    */
  def findMatchingPassword(passwords: Seq[String], hash: String): Future[Option[String]] = {
    val verified: Future[Seq[Boolean]] = Future.sequence(passwords.map(bcrypt.verify(_, hash)))
    val a: Future[Seq[(String, Boolean)]] = verified.map(seq => passwords.zip(seq))
    a.map(pair => pair.find { case (_, flag) => flag } match {
      case Some((password, _)) => Some(password)
      case _ => None
    })
  }

  /**
    * логирует начало и окончание выполнения Future, и продолжительность выполнения
    */
  def withLogging[A](tag: String)(f: => Future[A]): Future[A] = {
    val start = System.currentTimeMillis()
    logger.trace(s"$tag starts at $start")
    f.onComplete(_ => {
      val end = System.currentTimeMillis()
      logger.trace(s"$tag finish at $end")
    })
    f
  }

  /**
    * пытается повторно выполнить f retries раз, до первого успеха
    * если все попытки провалены, возвращает первую ошибку
    *
    * Важно: f не должна выполняться большее число раз, чем необходимо
    */
  def withRetry[A](f: => Future[A], retries: Int): Future[A] = {
    def retry(F: => Future[A], counter: Int, firstError: Option[Throwable] = None): Future[A] =
      F.recoverWith {
        case er if retries == 0 => Future.failed(er)
        case er if counter == retries => retry(F, counter - 1, Some(er))
        case _ if counter == 0 => Future.failed(firstError.get)
        case _ => retry(F, counter - 1, firstError)
      }

    retry(f, retries)
  }

  /**
    * по истечению таймаута возвращает Future.failed с java.util.concurrent.TimeoutException
    */
  def withTimeout[A](f: Future[A], timeout: FiniteDuration): Future[A] = {
    val p = Promise[A]()
    f.map(a => p.success(a)).recover { case exeption => p.failure(exeption)}
    Scheduler.executeAfter(timeout)(p.failure(new TimeoutException))
    p.future
  }


  /**
    * делает то же, что и hashPasswordList, но дополнительно:
    *   - каждая попытка хеширования отдельного пароля выполняется с таймаутом
    *   - при ошибке хеширования отдельного пароля, попытка повторяется в пределах retries (свой на каждый пароль)
    *   - возвращаются все успешные результаты
    */
  def hashPasswordListReliably(passwords: Seq[String], retries: Int, timeout: FiniteDuration): Future[Seq[(String, String)]] = {
    val hashes: Seq[Future[String]] = passwords.map(pass => withRetry(withTimeout(bcrypt.hash(pass), timeout), retries))

    val sTryes: Seq[Future[Try[String]]] = hashes.map(elem => elem.map(Success(_)).recover {
      case exep => Failure(exep)
    })

    val futureSTryes = Future.sequence(sTryes)

    val tryesZipPasswords: Future[Seq[(String, Try[String])]] = futureSTryes.map(passwords.zip(_))

    tryesZipPasswords.map(_.collect { case (a, Success(b)) => (a, b) })
  }
}
