package assignment

import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus._
import store.AsyncCredentialStore

import scala.concurrent.Future

import scala.concurrent.ExecutionContext.Implicits.global


class ConfigCredentialStore(config: Config) extends AsyncCredentialStore {
  private val credentialMap = config.as[Map[String, String]]("credentials")

  /**
    * возвращает хеш пользовательского пароля
    */
  override def find(user: String): Future[Option[String]] = Future {credentialMap.get(user)}
}